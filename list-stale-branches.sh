remove_stale_branches(){
  PROJECT_ID=$1
  PROJECT_NAME=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID" | jq -r '.name')
  # Iterate through the list of branches and delete each one
  for BRANCH in $(curl -s --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/repository/branches" | jq '.[].name')
  do
    BRANCH=$(echo $BRANCH | tr -d '"')
    [[ $BRANCH == *"/"* ]] && continue
    [[ $BRANCH == "main" ]] && continue
    [[ $BRANCH == "master" ]] && continue

    last_commit_date=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/repository/branches/${BRANCH}" | jq -r '.commit.committed_date')
    blame=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/repository/branches/master" | jq -r '.commit.author_email')
    # Split date string into separate variables for year, month and day
    IFS="T" read -r date_year date_time <<< "$last_commit_date"
    IFS="-" read -r year month day <<< "$date_year"

    # Get current year, month and day
    current_year=$(date +"%Y")
    current_month=$(date +"%m")
    current_day=$(date +"%d")

    # Calculate the difference in months between the date string and the current date
    month_diff=$(( (current_year - year) * 12 + (current_month - month) ))

    # Check if the difference in months is greater than 1
    if [ $month_diff -gt 1 ]; then
      echo "$PROJECT_NAME / $BRANCH - $last_commit_date"
      echo "The date is more than one month ago"
      echo "send mail to $blame"
      echo ""
    fi
  done
}



API_PREFIX="https://gitlab.com/api/v4"
for group in $(cat /tmp/productivity/gitlab/projects/*); do
  for project_id in $(curl -sSf --request GET --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "$API_PREFIX/groups/$group/" | jq -r '.projects | .[] | select(.archived == false)' | jq -r '.id')
  do
    #echo $project_id
    remove_stale_branches $project_id
  done
done
